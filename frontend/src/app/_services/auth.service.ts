import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { environment as env } from 'src/environments/environment';
import { APIResponse,Jobs } from '../models';
const PRIVATE='https://naukaries.herokuapp.com/private/'
const PUBLIC='http://localhost:8080/api/myjobposts';
const PUBLICCOL='http://localhost:8080/api/mycolleges';
const AUTH_API = 'http://localhost:8080/api/auth/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const TOKEN_KEY = 'auth-token';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      username,
      password
    }, httpOptions);
  }

  register(username: string, email: string, password: string): Observable<any> {
    return this.http.post(AUTH_API + 'signup', {
      username,
      email,
      password
    }, httpOptions);
  }
  
   


public getToken(): string | null {
  return window.sessionStorage.getItem(TOKEN_KEY);
}

getpayload()
{
  let token=this.getToken();
  return JSON.parse(window.atob(TOKEN_KEY.split('.')[1])); 
  
}
private extractData(res: Response) {
  let body = res;
  return body || { };
}

getjobList():Observable<any>//: Observable<APIResponse<Jobs>>
{
  const httpOptions = {
   headers: new HttpHeaders({
      'Accept': 'application/json, text/plain, */*',
       'Content-Type':'application/json',
      'Authorization': 'Bearer '+this.getToken()
    })
  };
 // console.log(httpOptions);
  //employees/getjobs/
  //${this.getpayload().id}

 // return this.http.get<APIResponse<Jobs>>(`${PUBLIC}`,httpOptions);
 return this.http.get(`${PUBLIC}`)
}
applyjob(jobs:any):Observable<any>
{
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization': 'Bearer'+' '+this.getToken()
    })
  };
  //let job_id:any=jobs.jobDetails._id;
  //let emp_id:any=this.getpayload().id;
   return this.http.post(`${PUBLIC}/apply`,jobs,httpOptions);
}
getappliedjobs()
  {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':'application/json',
        'Authorization':'Bearer'+' '+this.getToken()
      })
    };
    return this.http.get(`${PUBLIC}/applied`,httpOptions);
  }
  uploadprofilepic(fd:any)
  {
    return this.http.post(`${PRIVATE}employee/uploadpicture/${this.getpayload().id}`,fd);
   
  }
  postjob(body:any):Observable<any>
{
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization': 'Bearer'+' '+this.getToken()
    })
  };
  return this.http.post(`${PUBLIC}/post`,body,httpOptions);
}
getpostedjobs()
{
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization':'Bearer'+' '+ this.getToken()
    })
  };
  return this.http.get(`${PUBLIC}/posted`,httpOptions);
}
logout()
{
  localStorage.removeItem('token');
  localStorage.removeItem('currentrecruiter');
  // localStorage.removeItem('currentemployeeid')
}
getprofile()
{
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization': `Bearer${this.getToken()}`
    })
  };
  return this.http.get(`${PUBLICCOL}/1`,httpOptions);
}

Empupdateprofile(body:any)
{
 /* return this.http.put(`${PUBLICCOL}/1/update`,body,
  {
    
      observe:'body',
      headers:new HttpHeaders().append('Content-Type','application/json')
    
  });*/
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':'application/json',
      'Authorization': 'Bearer'+' '+this.getToken()
    })
  };
  return this.http.put(`${PUBLICCOL}/1/update`,body,httpOptions);
}


searchbycompany(companyname)
{
  const httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json, text/plain, */*',
       'Content-Type':'application/json',
       'Authorization': 'Bearer'+' '+this.getToken()
    })
  };
  return this.http.get(`${PUBLIC}/${companyname}`,httpOptions);
}
searchbyrole(jobrole:any)
  {
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json, text/plain, */*',
         'Content-Type':'application/json',
        'Authorization': 'Bearer '+this.getToken()
      })
    };
    return this.http.get(`${PUBLIC}/${jobrole}`,httpOptions);
  }
 
}

/*function CrossOrigin(arg0: string, arg1: string) {
  throw new Error('Function not implemented.');
}*/
