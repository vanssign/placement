import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/_services/auth.service';

@Component({
  selector: 'app-recdashboard',
  templateUrl: './recdashboard.component.html',
  styleUrls: ['./recdashboard.component.css']
})
export class RecdashboardComponent implements OnInit {
  constructor(private router:Router,private activeroute:ActivatedRoute,private authService:AuthService) { }
  companyName:any;
  ngOnInit() {
    //this.companyName=this.authService.getpayload().companyName;
  }
  logoutRecruiter()
  {
    this.authService.logout();
    this.router.navigate(['login/rec_login'],)
  }
  applied_Employees()
  {
    this.router.navigate(['applied'],{relativeTo:this.activeroute});
  }
  posted_jobs()
  {
    this.router.navigate(['postedjobs'],{relativeTo:this.activeroute});
  }
  gotoprofilepage()
  {
    this.router.navigate(['recruiter/rprofile']);
  }
  postjobs()
  {
    this.router.navigate(['postjobs'],{relativeTo:this.activeroute});
  }

}